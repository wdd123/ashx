var app = angular.module('myApp', ["common"]);

app.controller('deviceCtrl', [ '$scope', '$http', '$location', 'utils',
 		function($scope, $http, $location, utils) {
			$scope.bindDevice = false;
			$scope.deviceDetail = false;
 			$http({
 				method : 'GET',
 				url : utils.urlUtils.getBaseUrl() + '/ckDevice/myDevice'
 			}).then(function successCallback(response) {
 				if (response.data.result) {
 					$scope.device = response.data.result;
 					$scope.deviceDetail = true;
 				} else {
 					$scope.bindDevice = true;
 				}
 			}, function errorCallback(response) {
 			});
 			
 			$scope.bindUserDevice = function () {
 				$http({
 	 				method : 'POST',
 	 				url : utils.urlUtils.getBaseUrl() + '/ckDevice/bindDevice',
 	 				data : {deviceNumber: $scope.deviceNumber}
 	 			}).then(function successCallback(response) {
 	 				if (response.data.result) {
 	 					$scope.device = response.data.result;
 	 					$scope.bindDevice = false;
 	 					$scope.deviceDetail = true;
 	 				} else {
 	 					alert("设备编号不存在,请重新输入");
 	 				}
 	 			}, function errorCallback(response) {
 	 			});
 			};
 			
 			$scope.unbindUserDevice = function (deviceId) {
 				$http({
 	 				method : 'POST',
 	 				url : utils.urlUtils.getBaseUrl() + '/ckDevice/unbindDevice/' + deviceId,
 	 				data : {deviceNumber: $scope.deviceNumber}
 	 			}).then(function successCallback(response) {
 	 				if (response.data.result) {
 	 					alert("设备解绑成功!");
 	 					$scope.deviceDetail = false;
 	 					$scope.bindDevice = true;
 	 				} else {
 	 					alert("设备编号不存在或者解绑失败!");
 	 				}
 	 			}, function errorCallback(response) {
 	 			});
 			}
 } ]);