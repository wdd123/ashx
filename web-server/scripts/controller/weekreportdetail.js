var app = angular.module('myApp', ["common"]);

app.controller('weekReportDetailCtrl', [ '$scope', '$http', '$location', 'utils',
	function($scope, $http, $location, utils) {
		var weekRange = $location.search().weekRange;
		
		$scope.basic = {};
		
		$scope.init = function () {
			$scope.selectedWeek = weekRange;
			$scope.queryDateByWeek($scope.selectedWeek);
		};
		
		$scope.queryDateByWeek = function(weekRange) {
			$http({
				method : 'POST',
				url : utils.urlUtils.getBaseUrl() + '/history/weekReportDetail',
				data : {
					weekRange : weekRange
				}
			}).then(function successCallback(response) {
				$scope.basic = response.data.basic;
				if (response.data.result) {
					$scope.weekData = response.data.result;
				} else {
					alert("没有数据记录");
				}
			}, function errorCallback(response) {
			});
		};
		
		$scope.init();		
	} ]);