var app = angular.module('myApp', ["common", "checklist-model"]);

app.controller('myInfoCtrl', [ '$scope', '$http', '$location', 'utils',
		function($scope, $http, $location, utils) {
			//get the user info first
			$scope.patient = {};
			$scope.hyzk = [];
			$scope.sshzws = [];
			$scope.grxg = [];
			$scope.jwjbzz = [];
			$scope.jzbs = [];
			$scope.swhzjcwgm = [];
			$scope.syzk = [];
			$scope.ywbs = [];
			$scope.cqhzzjyw = [];
			$http({
				method : 'GET',
				url : utils.urlUtils.getBaseUrl() + '/ckUserPatient/queryPatient'
			}).then(function successCallback(response) {
				if (response.data.result) {
					$scope.patient = response.data.result.patient;
					$scope.hyzk = response.data.result.hyzk;
					$scope.sshzws = response.data.result.sshzws;
					$scope.grxg = response.data.result.grxg;
					$scope.jwjbzz = response.data.result.jwjbzz;
					$scope.jzbs = response.data.result.jzbs;
					$scope.swhzjcwgm = response.data.result.swhzjcwgm;
					$scope.syzk = response.data.result.syzk;
					$scope.ywbs = response.data.result.ywbs;
					$scope.cqhzzjyw = response.data.result.cqhzzjyw;
					if ($scope.patient.birthDate) {
						$scope.birthDate = new Date($scope.patient.birthDate);
					}
				}
			}, function errorCallback(response) {
			});
			
			var keyForNone = 1; // 暂无选项选择后，其它项不能选
			$scope.clickOnCheckBoxInput = function (checkModel, checked, key) {
				if (key === keyForNone && checked) {
					// remove all others items in checkModel, only keep 1
					for (var i = 0; i < checkModel.length; i++) {
				        if (checkModel[i] !== keyForNone) {
				        	var isItemRemoved = remove(checkModel, checkModel[i]);
				        	if (isItemRemoved) {
				        		i--;
				        	}
				        }
				     }
				}
			};
			
			// remove item from array , return true when one item is removed
			function remove(arr, item) {
			    if (angular.isArray(arr)) {
			      for (var i = 0; i < arr.length; i++) {
			        if (arr[i] === item) {
			          arr.splice(i, 1);
			          return true;
			        }
			      }
			    }
			    return false;
			 }
			
			$scope.isItemNotClickAble = function (checkModel, key) {
				return key !== keyForNone && checkModel && checkModel.indexOf(keyForNone) !== -1;
			};
			
			$scope.validateForm = function() {
				if (!$scope.patient.fullName) {
					alert("请输入姓名");
					return false;
				}
				if (!$scope.patient.mobilePhone) {
					alert("请输入手机号码");
					return false;
				}
				return true;
			};
			
			$scope.submitForm = function () {
					if (!$scope.validateForm()) {
						return;
					}
					$scope.patient.birthDate = $scope.birthDate;
					$http({
						method : 'POST',
						url : utils.urlUtils.getBaseUrl() + '/ckUserPatient/savePatient',
						data : $scope.patient
					}).then(function successCallback(response) {
						if (response.data.result) {
							$scope.user = response.data.result;
							alert("保存成功");
						} else {
							alert("保存失败");
						}
					}, function errorCallback(response) {
					});
				}
		} ]);