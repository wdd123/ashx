var app = angular.module('myApp', ["common", "easypiechart"]);

app.controller('siteCtrl', [ '$scope', '$http', '$location', 'utils', 'constants',
		function($scope, $http, $location, utils, constants) {
			var id = $location.search().id;
			var timeId = $location.search().timeId;
			$scope.tempChart = {};
			$scope.oxygenChart = {};
			$scope.flowChart = {};
			$scope.totalTimeChart = {};
			
			$scope.basic = {};
			var currentDate = new Date();

            $http({
                method : 'GET',
                url : utils.urlUtils.getBaseUrl() + '/ckUser/checkIsHaveExtendInfo'
                // url : '/web-server/ckUser/checkIsHaveExtendInfo'
            }).then(function successCallback(response) {
                if(response.data.result == true){
                    $http({
                        method : 'GET',
                        url : utils.urlUtils.getBaseUrl() + '/ck/deviceData/' + id + '/' + timeId + "?timestamp=" + currentDate.getTime()
                    }).then(function successCallback(response) {
                        $scope.basic = response.data.basic;
                        if (response.data.result) {
                            $scope.deviceData = response.data.result;
                            $scope.generateChartValue($scope.deviceData);
                            $scope.tempChartReady = true;
                            $scope.oxygenChartReady = true;
                            $scope.flowChartReady = true;
                            $scope.timeChartReady = true;
                        } else {
                            alert("没有数据记录，可能是设备没有联网，或者氧疗设备未开启");
                        }
                    }, function errorCallback(response) {
                    });
			
					$scope.getChartObjForOxygen = function (oxygenValue) {
						var chartObj = {};
						if (oxygenValue ) {
							if (oxygenValue < 72) {
								chartObj.color = constants.chartColors.red;
								chartObj.text = constants.chartTextForOxygen.bad;
							} else if (oxygenValue >= 72 && oxygenValue < 82) {
								chartObj.color = constants.chartColors.yellow;
								chartObj.text = constants.chartTextForOxygen.normal;
							} else if (oxygenValue >= 82 && oxygenValue < 93) {
								chartObj.color = constants.chartColors.blue;
								chartObj.text = constants.chartTextForOxygen.good;
							} else if (oxygenValue >= 93) {
								chartObj.color = constants.chartColors.green;
								chartObj.text = constants.chartTextForOxygen.very_good;
							}
							chartObj.percent = oxygenValue;
						}
						return chartObj;
					};

					$scope.getChartObjForFlow = function (flowValue) {
						var chartObj = {};
						var maxFlowValue = 10;
						if (flowValue ) {
							if (flowValue < 1) {
								chartObj.color = constants.chartColors.red;
								chartObj.text = constants.chartTextForFlow.bad;
							} else if (flowValue >= 1 && flowValue < 3) {
								chartObj.color = constants.chartColors.yellow;
								chartObj.text = constants.chartTextForFlow.normal;
							} else if (flowValue >= 3 && flowValue < 5) {
								chartObj.color = constants.chartColors.blue;
								chartObj.text = constants.chartTextForFlow.good;
							} else if (flowValue >= 5) {
								chartObj.color = constants.chartColors.green;
								chartObj.text = constants.chartTextForFlow.very_good;
							}
							if (flowValue <= maxFlowValue) {
								chartObj.percent = (flowValue/maxFlowValue) * 100;
							} else {
								chartObj.percent = 100;
							}
						}
						return chartObj;
					};

					$scope.getChartObjForTotalTime = function (totalTimeValue) {
						var chartObj = {};
						if (totalTimeValue ) {
							if (totalTimeValue < 8) {
								chartObj.color = constants.chartColors.yellow;
								chartObj.text = constants.chartTextForTotalTime.normal;
							} else if (totalTimeValue >= 8 && totalTimeValue < 16) {
								chartObj.color = constants.chartColors.blue;
								chartObj.text = constants.chartTextForTotalTime.good;
							} else if (totalTimeValue >= 16) {
								chartObj.color = constants.chartColors.green;
								chartObj.text = constants.chartTextForTotalTime.very_good;
							}
							if (totalTimeValue <= 24) {
								chartObj.percent = (totalTimeValue/24) * 100;
							} else {
								chartObj.percent = 100;
							}
						}
						return chartObj;
					};

					$scope.getChartObjForTemp = function (tempValue) {
						var chartObj = {};
						if (tempValue || tempValue <= 0 ) {
							if (tempValue < 0) {
								chartObj.color = constants.chartColors.yellow;
								chartObj.text = constants.chartTextForTemp.normal;
							} else if (tempValue >= 0 && tempValue < 35) {
								chartObj.color = constants.chartColors.blue;
								chartObj.text = constants.chartTextForTemp.good;
							} else if (tempValue >=35) {
								chartObj.color = constants.chartColors.green;
								chartObj.text = constants.chartTextForTemp.very_good;
							}
							if (tempValue <= 40) {
								chartObj.percent = (tempValue/40) * 100;
							} else {
								chartObj.percent = 100;
							}
						}
						return chartObj;
					};

					$scope.generateChartValue = function(deviceData) {
						var chartObjForOxygen = $scope.getChartObjForOxygen(deviceData.avgOxygenValue);
						var chartObjForFlow = $scope.getChartObjForFlow(deviceData.avgFlowValue);
						var chartObjForTotalTime = $scope.getChartObjForTotalTime(deviceData.totalTimeValue);
						var chartObjForTemp = $scope.getChartObjForTemp(deviceData.avgTempValue);
						$scope.generateChartValueForOxygen(chartObjForOxygen);
						$scope.generateChartValueForFlow(chartObjForFlow);
						$scope.generateChartValueForTemp(chartObjForTemp);
						$scope.generateChartValueForTotalTime(chartObjForTotalTime);
					};

					$scope.generateChartValueForOxygen = function(chartObjForOxygen) {
						 $scope.oxygenChart.text_oxygen = chartObjForOxygen.text;
						 $scope.oxygenChart.percent_oxygen = chartObjForOxygen.percent;
						 $scope.oxygenChart.options_oxygen = {
							barColor: chartObjForOxygen.color,
							trackColor:"#ececec",
							scaleColor:false,
							scaleLength : 0,
							lineWidth:8,
							size: 100,
							lineCap:'round ',//circle
							animate:{ duration: 500, enabled: true }
						 };
					};

					$scope.generateChartValueForFlow = function(chartObjForFlow) {
						 $scope.flowChart.text_flow = chartObjForFlow.text;
						 $scope.flowChart.percent_flow = chartObjForFlow.percent;
						 $scope.flowChart.options_flow = {
							barColor: chartObjForFlow.color,
							trackColor:"#ececec",
							scaleColor:false,
							scaleLength : 0,
							lineWidth:8,
							size: 100,
							lineCap:'round ',//circle
							animate:{ duration: 1000, enabled: true }
						 };
					};

					$scope.generateChartValueForTotalTime = function(chartObjForTotalTime) {
						$scope.totalTimeChart.text_totalTime = chartObjForTotalTime.text;
						$scope.totalTimeChart.percent_totalTime = chartObjForTotalTime.percent;
						$scope.totalTimeChart.options_totalTime = {
							barColor: chartObjForTotalTime.color,
							trackColor:"#ececec",
							scaleColor:false,
							scaleLength : 0,
							lineWidth:8,
							size: 100,
							lineCap:'round ',//circle
							animate:{ duration: 1000, enabled: true }
						};
					};
					$scope.generateChartValueForTemp = function(chartObjForTemp) {
						$scope.tempChart.text_temp = chartObjForTemp.text;
						$scope.tempChart.percent_temp = chartObjForTemp.percent;
						$scope.tempChart.options_temp = {
							barColor: chartObjForTemp.color,
							trackColor:"#ececec",
							scaleColor:false,
							scaleLength : 0,
							lineWidth:8,
							size: 100,
							lineCap:'round ',//circle
							animate:{ duration: 1000, enabled: true }
						};
					};

                }else if(response.data.result == false){
                    window.location.href = 'personInfo.html'
                }
            }, function errorCallback(response) {
            });
		} ]);