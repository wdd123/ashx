var app = angular.module('myApp', ["common"]);

app.controller('weekReportCtrl', [ '$scope', '$http', '$location', 'utils', 'constants', '$filter',
		function($scope, $http, $location, utils, constants, $filter) {
		var totalWeeks = 30;
		$scope.pastWeeks = [];
		$scope.selectedWeek = {};
		
		$scope.basic = {};
		
		var oxygenChart;
		var flowChart;
		var tempChart;
		var timeChart;
		var selectedWeekKey = "weekReport_selected_week_key";
            $http({
                method : 'GET',
                url : utils.urlUtils.getBaseUrl() + '/ckUser/checkIsHaveExtendInfo'
                // url : '/web-server/ckUser/checkIsHaveExtendInfo'
            }).then(function successCallback(response) {
                if(response.data.result == true){
					$scope.generateWeeks = function() {
						var currentDate = new Date();
						var beginDate, endDate;
						for (var i=1; i<=totalWeeks;i++) {
							currentDate.setDate(currentDate.getDate() -1);
							endDate = new Date(currentDate.getTime());
							currentDate.setDate(currentDate.getDate() -6);
							beginDate = new Date(currentDate.getTime());
							$scope.pastWeeks.push({beginDate: $filter('date')(beginDate, 'yyyy-MM-dd'), endDate: $filter('date')(endDate, 'yyyy-MM-dd')})
						}
					};

					$scope.weekSelected = function() {
						$scope.queryDateByWeek($scope.selectedWeek);
					};

					$scope.init = function () {
						$scope.generateWeeks();
						$scope.selectedWeek = $scope.pastWeeks[0].beginDate+"~"+$scope.pastWeeks[0].endDate;
						$scope.queryDateByWeek($scope.selectedWeek);
					};

					var updateChart = function (chartObj, labels, datas) {
						chartObj.data.labels = labels;
						chartObj.data.datasets[0].data = datas;
						chartObj.update();
					};

					$scope.goToDetail = function() {
						window.location.href="weekreportdetail.html?weekRange=" + $scope.selectedWeek;
					};

					$scope.queryDateByWeek = function(weekRange) {
						$http({
							method : 'POST',
							url : utils.urlUtils.getBaseUrl() + '/history/weekReport',
							data : {
								weekRange : weekRange
							}
						}).then(function successCallback(response) {
							$scope.basic = response.data.basic;
							if (response.data.result) {
								$scope.weekData = response.data.result;
								if ($scope.weekData.oxygenChart) {
									if (oxygenChart) {
										updateChart(oxygenChart, $scope.weekData.oxygenChart.labels, $scope.weekData.oxygenChart.datas);
									} else {
										oxygenChart = $scope.generateChartReport('myChartOxygen', $scope.weekData.oxygenChart.labels, $scope.weekData.oxygenChart.datas, '氧浓度');
									}
								}
								if ($scope.weekData.flowChart) {
									if (flowChart) {
										updateChart(flowChart, $scope.weekData.flowChart.labels, $scope.weekData.flowChart.datas);
									} else {
										flowChart = $scope.generateChartReport('myChartFlow', $scope.weekData.flowChart.labels, $scope.weekData.flowChart.datas, '氧流量', "line", 1);
									}
								}
								if ($scope.weekData.tempChart) {
									if (tempChart) {
										updateChart(tempChart, $scope.weekData.tempChart.labels, $scope.weekData.tempChart.datas);
									} else {
										tempChart = $scope.generateChartReport('myChartTemp', $scope.weekData.tempChart.labels, $scope.weekData.tempChart.datas, '氧温度');
									}
								}
								if ($scope.weekData.timeChart) {
									if (timeChart) {
										updateChart(timeChart, $scope.weekData.timeChart.labels, $scope.weekData.timeChart.datas);
									} else {
										timeChart = $scope.generateChartReport('myChartTime', $scope.weekData.timeChart.labels, $scope.weekData.timeChart.datas, '氧用时', "bar");
									}
								}
							} else {
								alert("没有数据记录");
							}
						}, function errorCallback(response) {
						});
					};

					$scope.generateChartReport = function(chartIdInDom, labels, datas, dataLabel, chartType, fillValue) {
						var type = "line";
						var fill = false;
						if (chartType) {
							type = chartType;
						}
						if (fillValue) {
							fill = fillValue;
						}
						var config = {
								type: type,
								data: {
									labels: labels,
									datasets: [{
										label: dataLabel,
										backgroundColor: constants.chartColors.blue,
										borderColor: constants.chartColors.blue,
										data: datas,
										fill: fill
									}]
								},
								options: {
									responsive: false,
									title:{
										display:false
									},
									scales: {
										xAxes: [{
											display: true,
											scaleLabel: {
												display: false,
												labelString: ''
											}
										}],
										yAxes: [{
											display: true,
											ticks: {
												beginAtZero:true
											},
											scaleLabel: {
												display: false,
												labelString: ''
											}
										}]
									}
								}
							};
							var ctx = document.getElementById(chartIdInDom).getContext("2d");
							var chartObj = new Chart(ctx, config);
							return chartObj;
						}
					$scope.init();
			}else if(response.data.result == false){
				window.location.href = 'personInfo.html'
			}
		}, function errorCallback(response) {
		});
	} ]);