var app = angular.module('myApp', ["common"]);

app.controller('myqrCodeController', [ '$scope', '$http', '$location', 'utils',
 		function($scope, $http, $location, utils) {
			$scope.myQrcodeUrl = "";
			$scope.myUsers = [];
 			$http({
 				method : 'GET',
 				url : utils.urlUtils.getBaseUrl() + '/ckUser/myQrcodeInfo'
 			}).then(function successCallback(response) {
 				if (response.data.result) {
 					$scope.myQrcodeUrl = response.data.result.qrCodeUrl;
 					$scope.myUsers = response.data.result.myUsers;
 				}
 			}, function errorCallback(response) {
 			});
 } ]);