angular.module("common", [])
	.constant("constants", {
		appName : "chukang",
		chartColors : {
			green: '#5cb85c',
			blue: '#5bc0de',
			yellow: '#f0ad4e',
			red:'#d9534f'
		},
		chartTextForOxygen : {
			very_good: '氧浓度很好',
			good:'氧浓度正常',
			normal: '氧浓度偏低',
			bad:'氧浓度很低'
		},
		chartTextForFlow : {
			very_good: '氧流量很高',
			good:'氧流量偏高',
			normal: '氧流量正常',
			bad:'氧流量偏低'
		},
		chartTextForTemp : {
			very_good: '氧温度偏高',
			good:'氧温度正常',
			normal: '氧温度偏低',
			bad:'氧温度偏低'
		},
		chartTextForTotalTime : {
			very_good: '时间较长',
			good:'时间一般',
			normal: '时间偏少',
			bad:'时间偏少'
		}
	})
	.config(["$locationProvider", function($locationProvider) {
		$locationProvider.html5Mode({
			enabled : true,
			requireBase : false
		}).hashPrefix('!');
	}])
	.config([ "$httpProvider", function($httpProvider) {
		$httpProvider.interceptors.push("commonHttpInterceptor");
		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
		// Override $http service's default transformRequest
		$httpProvider.defaults.transformRequest = [function(data) {
			/**
			 * The workhorse; converts an object to x-www-form-urlencoded serialization.
			 */
			var param = function(obj) {
				var query = '';
				var name, value, fullSubName, subName, subValue, innerObj, i;

				for (name in obj) {
					value = obj[name];

					if (value instanceof Array) {
						for (i = 0; i < value.length; ++i) {
							subValue = value[i];
							fullSubName = name + '[' + i + ']';
							innerObj = {};
							innerObj[fullSubName] = subValue;
							query += param(innerObj) + '&';
						}
					} else if (value instanceof Date) {
						query += encodeURIComponent(name) + '='
						+ encodeURIComponent(value) + '&';
					} else if (value instanceof Object) {
						for (subName in value) {
							subValue = value[subName];
							fullSubName = name + '[' + subName + ']';
							innerObj = {};
							innerObj[fullSubName] = subValue;
							query += param(innerObj) + '&';
						}
					} else if (value !== undefined && value !== null) {
						query += encodeURIComponent(name) + '='
								+ encodeURIComponent(value) + '&';
					}
				}

				return query.length ? query.substr(0, query.length - 1) : query;
			};

			return angular.isObject(data) && String(data) !== '[object File]'
					? param(data)
					: data;
		}];
	} ])
	.factory("utils", ["$location", function($location) {
		var pendingUrl;// url need to be authenticated
		return {
			tokenUtils : {
				get : function() {
					return window.localStorage.getItem("token");
				},
				set : function(token) {
					window.localStorage.setItem("token", token);
				}
			},
			storageUtils : {
				get : function(key) {
					return window.localStorage.getItem(key);
				},
				getAndClear : function(key) {
					var result = window.localStorage.getItem(key);
					window.localStorage.setItem(key, "");
					return result;
				},
				set : function(key, value) {
					window.localStorage.setItem(key, value);
				}
			},
			urlUtils : {
				get : function() {
					var tempUrl = pendingUrl;
					pendingUrl = "";
					return tempUrl;
				},
				set : function(url) {
					pendingUrl = url;
				},
				getBaseUrl : function() {
					var app_host = $location.protocol() + "://" + $location.host();
					if ($location.port()) {
						app_host += ":" + $location.port();
					}
					var app_name = $location.url().substring(1, $location.url().indexOf("/", 1));
					app_host += "/" + app_name;
					return app_host;
				}
			}
		}
	} ])
	.factory("commonHttpInterceptor", ["$q","$rootScope","$window","utils","$location",
				function($q, $rootScope, $window, utils, $location) {
					return {
						request : function(config) {
							var authToken = utils.tokenUtils.get();
							if (config.method==='GET') {
								var params = config.params;
								if (!params) {
									params = {auth_code : $location.search().code, auth_token: authToken};
								} else {
									params.auth_code = $location.search().code;
									params.auth_token = authToken;
								}
								config.params = params;
							} else {
                                var data = config.data;
								if (!data) {
									data = {auth_code : $location.search().code, auth_token : authToken};
								}else {
									data.auth_code = $location.search().code;
									data.auth_token = authToken;
								}
                                config.data = data;
							}
							return config;
						},
						response : function(response) {
							// handle for general error like auth
							// failed.
							var contentType = response.headers("Content-Type");
							var current_url = $location.absUrl();
							if (contentType && contentType.startsWith("application/json")) {
								var user_token = response.headers("user_token");
								if (user_token) {
									utils.tokenUtils.set(user_token);
								}
								var resCode = response.data.resCode;
								if (resCode === "0") {
									// success response and refresh auth
									// token
									var pendingUrl = utils.urlUtils.get();
									utils.tokenUtils.set(response.data.result);
									if (pendingUrl) {
										window.location.href = pendingUrl;
										return $q.reject(response);
									} else {
										return response;
									}
								} else if (resCode === "1") {
									// just success response
									// utils.tokenUtils.set(response.data.token);
									return response;
								} else if (resCode === "9") {
									var errMsg = response.data.errMsg;
									window.alert(errMsg);
									return $q.reject(response);
									//window.location.reload();
								} else {
									utils.urlUtils.set(current_url);
									utils.tokenUtils.set("");
									var appId = "wxf56d8ee069286fe1";
									var redirect_url = encodeURIComponent(current_url);
									//redirect_url = "http://weixin.chukang.xin/test";// test for local to get code.
									var direct_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=";
									direct_url += appId + "&redirect_uri=";
									direct_url += redirect_url + "&response_type=code&scope=snsapi_userinfo&state=state#wechat_redirect";
									window.location.href = direct_url;
									return $q.reject(response);
								}
							} else {
								return response;
							}

						},
						responseError : function(response) {
							return response;
						}
					}
				} ])
		.run([ "$rootScope", function($rootScope) {
		$rootScope.appScope = {};
	} ]);