var app = angular.module('myApp', ["common"]);

app.controller('siteCtrl', [ '$scope', '$http', '$location', 'utils',
		function($scope, $http, $location, utils) {
			var id = $location.search().id;
			var timeId = $location.search().timeId;
			$http({
				method : 'GET',
				url : utils.urlUtils.getBaseUrl() + '/ck/deviceData/' + id + '/' + timeId
			}).then(function successCallback(response) {
				if (response.data.result) {
					$scope.deviceData = response.data.result;
					$scope.createCharts();
				} else {
					alert("没有数据记录");
				}
			}, function errorCallback(response) {
			});
			$scope.createCharts = function () {
				var ctx = document.getElementById("pieChart-totalTime").getContext("2d");
				var config = {
					    type: 'pie',
					    data: {
					        datasets: [{
					            data: [
					                20,
					                30,
					                50
					            ],
					            backgroundColor: [
					                "#F7464A",
					                "#46BFBD",
					                "#FDB45C"
					            ],
					        }],
					        labels: [
					            "Principal Amount",
					            "Interest Amount",
					            "Processing Fee"
					        ]
					    },
					    options: {
					        responsive: true
					    }
					};

				window.myPie = new Chart(ctx, config);
		        
		        var config2 = {
			            type: 'pie',
			            data: {
					        datasets: [{
					            data: [
					                10,
					                20,
					                70
					            ],
					            backgroundColor: [
					                "#F7464A",
					                "#46BFBD",
					                "#FDB45C"
					            ],
					        }],
					        labels: [
					            "Principal Amount",
					            "Interest Amount",
					            "Processing Fee"
					        ]
					    },
					    options: {
					        responsive: true
					    }
				};
		        var ctx2 = document.getElementById("pieChart-avgFlow").getContext("2d");
		        window.pieChart1 = new Chart(ctx2, config2);
			};
		} ]);